# Recalcula boleto vencido

O propósito deste simples programa em Python é recalcular o boleto vencido aplicando o percentual da multa e juros sobre o valor do mesmo.

Para executar basta entrar na pasta onde está o código fonte e executar:

```
$ python3 boleto-vencido.py
```

## Copyright and license
Code Copyright 2019 Wilton Gonçalves. Code released under the MIT license.
